# Desafio Frontend CARGOBR

Olá Dev! Tudo bem?

Esse é o desafio técnico para a vaga de Front-End Developer aqui na CARGOBR, nós estamos em busca de uma pessoa acima de tudo interessada, com boa capacidade de aprendizado, adaptação e principalmente bom senso!

O objetivo desse teste é avaliar suas competências técnicas e também te desafiar, como o próprio nome diz. Não é obrigatório fazer tudo que foi proposto, este é apenas um guia que vai servir para reconhecer seu esforço, vamos avaliar também o seu potencial para aprender, se adaptar e tomar decisões.

Vamos ao teste!

## Missão

Implementar uma aplicação onde será possível listar itens, selecionar um ou mais itens e no final "comprar" esse item.

Use a [api do Github](https://developer.github.com/v3/) para listar os membros de qualquer organização, e cada membro será um "item" a ser "comprado". Você pode definir o valor de cada item a partir de informações do seu perfil do GitHub, como por exemplo: followers, repos, stars, commits, etc.

## Regras

Como dito acima esse é apenas um guia do que você pode entregar em seu desafio:

- _React_: essa é a nossa principal tecnologia utilizada no time de Front-End então seria ótimo se você a utilizasse.
- _Git_: nós iremos trabalhar com git diariamente, então seria legal se você organizasse a sua aplicação em um repositório público para compartilhar com a gente depois.
- _Testes_: uma aplicação bem feita é uma aplicação testada, certo ? Então esse é a hora de você colocar isso em prática, pode ser teste unitário ou de end-to-end, você quem manda.
- _Layout_: não temos um layout desenhado então use a sua criatividade para isso, você fica livre caso queira utilizar algum framework css de sua preferência.

## Stack utilizada pela CargoBR

Essa é a stack utilizada pelo front-end da CargoBR, você pode aplicar o mesmo em seu teste, isso te dará mais pontos ;)

- React
- Redux
- Styled-components
- Storybook
- Jest + Enzyme para teste unitário
- Cypress para teste end-to-end
- Eslint + prettier para formatação de código

## Enviando seu desafio

- Faça um fork *privado* desse repositório e crie uma nova branch com o seu nome, para que possamos te identificar (ex: nome_sobrenome).

- Adicione os e-mails informados como colaboradores do projeto
	- Vá em Repository settings do lado esquerdo da página principal do repositório;
	- Depois User and group access;
	- Adicione os e-mails na seção Users dessa tela;
	- Uma mensagem informando que um convite foi enviado será exibida.

- Deixe a mágica acontecer. Crie a sua aplicação React e desenvolva o desafio conforme achar melhor.

- Sinta-se a vontade para modificar o README.md para adicionar instruções de desenvolvimento (como instalar as dependências e rodar o projeto ?) ou qualquer outro comentário sobre a sua aplicação.

- Após terminar o desafio, você pode solicitar um Pull Request da sua branch para a branch master desse repositório. Vamos receber a sua solicitação e fazer a avaliação de seu código.

- Nós entraremos em contato para te dar um feedback e falar sobre as próximas etapas do processo seletivo!

## Boa sorte!
